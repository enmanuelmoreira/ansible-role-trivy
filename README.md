# Ansible Role: Trivy

This role installs [Trivy](https://aquasecurity.github.io/trivy/) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Installing

The role can be installed by running the following command:

```bash
git clone https://gitlab.com/enmanuelmoreira/ansible-role-trivy enmanuelmoreira.trivy
```

Add the following line into your `ansible.cfg` file:

```bash
[defaults]
role_path = ../
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    trivy_version: latest # tag v0.22.0 if you want a specific version
    trivy_arch: 64bit # 64bit, 32bit, ARM, ARM64, PPC64LE
    setup_dir: /tmp
    trivy_bin_path: /usr/local/bin/trivy
    trivy_templates_folder: /usr/local/share
    trivy_repo_path: https://github.com/aquasecurity/trivy/releases/download

This role can install the latest or a specific version. See [available Trivy releases](https://aquasecurity.github.io/trivy/releases/) and change this variable accordingly.

    trivy_version: latest # tag v0.22.0 if you want a specific version

The path of the trivy repository.

    trivy_repo_path: https://aquasecurity.github.io/trivy/releases/download

The path to the home Trivy directory.

    trivy_bin_path: /usr/local/bin/trivy

Trivy supports 64bit, 32bit, ARM, ARM64 and PPC64LE CPU architectures, just change for the main architecture of your CPU.

    trivy_arch: 64bit # 64bit, 32bit, ARM, ARM64, PPC64LE

Trivy needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install Trivy. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: enmanuelmoreira.trivy

## License

MIT / BSD
